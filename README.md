### Randomly sampling ROIs ###
A [Fiji](http://fiji.sc) script to randomly create circular ROIs within an existing freehand selection.

The code and example files here support [a blog post]( https://postacquisition.wordpress.com/2018/06/21/random-access-memoroi/) explaining the problem and the approach to solving it.

This was originally spurred by a question posted on the [ImageJ Forum](http://forum.imagej.net/t/random-generation-of-rois-in-a-selected-area/11286).

### Usage ###
There are two version of the script: `radialPositions.ijm` and `uniformPositions.ijm`. The former is provided for interest only and has limited user-friendlyness and error catching. The latter is (slightly) more polished and requires input of a TIF image containing an initial freehand (or segmented) ROI saved into the file as an overlay. You will be asked for the size (in pixels) and the number of the ROIs you wish to create.

The script will display your original image with the overlay and ROIs provided as selections in the ROI manager (below is an example of 4 spots each 50 pixels diameter).

![watermark](exampleOutput.PNG)

If the script cannot find a good fit in 20 iterations, an error message is produced and the script cancels.

### Acknowledgement ###
Written by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell Imaging](http://cci.liv.ac.uk).