spotDiam=50;
numSpots=2;
verbose=1;

//-- Set up by clearing workspace
print("\\Clear");
close("*");

//-- load the example image and convert the overlay to a selection
open("Q:\\ImageAnalysis\\2018-06-05-IJForum_randomROIs\\example.tif");
roiManager("reset");
run("To ROI Manager");
run("Remove Overlay");
roiManager("select", 0);

//-- Make the shape simpler
run("Fit Spline");
roiManager("reset");
run("Add Selection...");

getSelectionCoordinates(x,y);
//-- Find the centroid
List.clear;
List.setMeasurements;
cenX=List.getValue("X");
cenY=List.getValue("Y");
List.clear;

for (i=0;i<numSpots;i++){
//-- Pick a random spline point
splineIndex=floor(random() * x.length);
if (verbose>0){print("ROI #"+IJ.pad(i+1,2)+" Using spline point "+splineIndex+" Coordinates: "+d2s(x[splineIndex],0)+" / "+d2s(y[splineIndex],0));}
//-- Pick a random distance along the length of the line in x and y taking into account the size of the ROI

if (x[splineIndex]>cenX){randX=random*((x[splineIndex]-cenX-(spotDiam/2))); } else {randX=random*((x[splineIndex]-cenX+(spotDiam/2)));}
if (y[splineIndex]>cenY){randY=random*((y[splineIndex]-cenY-(spotDiam/2))); } else {randY=random*((y[splineIndex]-cenY+(spotDiam/2)));}

//-- Find coordinates of the new position
newX=cenX+randX;
newY=cenY+randY;
if (verbose>0){print("ROI #"+IJ.pad(i+1,2)+" New ROI coordinates: "+newX+" / "+newY);}

run("Specify...", "width="+spotDiam+" height="+spotDiam+" x="+newX+" y="+newY+" oval centered");
run("Add to Manager");

//-- check if the newly created selection overlaps (only on second selection and higher)
if (i>0){
roiManager("Combine"); //-- combine all selections
List.clear;
List.setMeasurements;
if (verbose>0){print("ROI #"+IJ.pad(i+1,2)+" Total Area = "+List.getValue("Area")+" Expected = "+((i+1)*sSpotArea));}

if (List.getValue("Area")<(i+1)*PI*pow((spotDiam/2),2)){
	//-- we have overlap so remove the ROI and go back a step
	if (verbose>0){print("Overlap found!: Expected area = "+((i+1)*sSpotArea)+" Found Area = "+List.getValue("Area"));}
	roiManager("select", i);
	roiManager("Delete");
	i--;


} //-- check second ROI only loop

}else{
	//-- First spot so record the area
	roiManager("select", 0);
	List.clear;
	List.setMeasurements;
	sSpotArea=List.getValue("Area");
	List.clear;
	if (verbose>0){print("ROI #"+IJ.pad(i+1,2)+" Single Spot area = "+sSpotArea);}

}
run("Select None");
} //-- NumSpots loop